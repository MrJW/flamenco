// package website contains references to the Flamenco website.
// Constants defined here can be used in the rest of Flamenco, for example for log messages.
package website

const (
	DocVariablesURL           = "https://flamenco.blender.org/usage/variables/blender/"
	WorkerCredsUnknownHelpURL = "https://flamenco.blender.org/faq/#what-does-unknown-worker-is-trying-to-communicate-mean"
	CannotFindManagerHelpURL  = "https://flamenco.blender.org/faq/#my-worker-cannot-find-my-manager-what-do-i-do"
	BugReportURL              = "https://flamenco.blender.org/get-involved"
	ShamanRequirementsURL     = "https://flamenco.blender.org/usage/shared-storage/shaman/#requirements"
	WorkerConfigURL           = "https://flamenco.blender.org/usage/worker-configuration/"
	OOMScoreAdjURL            = WorkerConfigURL
)
