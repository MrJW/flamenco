/**
 * Flamenco manager
 * Render Farm manager API
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import LifeCycleEventType from './LifeCycleEventType';

/**
 * The EventLifeCycle model module.
 * @module model/EventLifeCycle
 * @version 0.0.0
 */
class EventLifeCycle {
    /**
     * Constructs a new <code>EventLifeCycle</code>.
     * Flamenco life-cycle event, for things like shutting down, starting up, etc. 
     * @alias module:model/EventLifeCycle
     * @param type {module:model/LifeCycleEventType} 
     */
    constructor(type) { 
        
        EventLifeCycle.initialize(this, type);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, type) { 
        obj['type'] = type;
    }

    /**
     * Constructs a <code>EventLifeCycle</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EventLifeCycle} obj Optional instance to populate.
     * @return {module:model/EventLifeCycle} The populated <code>EventLifeCycle</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new EventLifeCycle();

            if (data.hasOwnProperty('type')) {
                obj['type'] = LifeCycleEventType.constructFromObject(data['type']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/LifeCycleEventType} type
 */
EventLifeCycle.prototype['type'] = undefined;






export default EventLifeCycle;

