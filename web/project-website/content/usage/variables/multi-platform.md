---
title: Two-way Variables for Multi-Platform Support
---

Two-way variables are there to support mixed-platform Flamenco farms. Basically
they perform *path prefix replacement*. This feature ensures that file paths are correctly interpreted across different operating systems, such as Linux, Windows, and macOS.

Let's look at an example configuration:

```yaml
variables:
  my_storage:
    is_twoway: true
    values:
    - platform: linux
      value: /media/shared/flamenco
    - platform: windows
      value: F:\flamenco
    - platform: darwin
      value: /Volumes/shared/flamenco
```

The difference with regular variables is that regular variables are one-way:
`{variablename}` is replaced with the value, and that's it. You only need to configure the variable in `flamenco-manager.yaml` file. Flamenco will do the rest automatically.

Two-way variables go both ways, as follows:

1. Job Submission:
  - When submitting a **job**, values **in the javascript jobs' command** are replaced
  with the corresponding variables as it's executed on the client. 
  - For example, if a job is submitted from a Windows machine, a path like `F:\flamenco\project.blend` will be recognized and stored as `{my_storage}/project.blend`.

2. Task Execution:
  - When sending a task to a worker, variables are replaced with values again.
  - For instance, a Linux worker receiving the task will see the path as `/media/shared/flamenco/project.blend`.

*(Do keep in mind that if you perform changes to a job, you'll need to re-submit*
*it.)*

This may seem like a lot of unnecessary work. After all, why go through the
trouble of replacing in one direction, when later the opposite is done? The
power lies in the fact that each replacement step can target a different
platform. In the first step the value for Linux can be recognised, and in the
second step the value for Windows can be put in its place.

Let's look at a more concrete example, based on the configuration shown above.

- Alice runs Blender on **macOS**. She submits a job that has its render output set
  to `/Volumes/shared/flamenco/renders/shot_010_a_anim`.
- Flamenco recognises the path, and stores the job as rendering to
  `{my_storage}/renders/shot_010_a_anim`.
- Bob's computer is running the Worker on **Windows**, so when it receives a render
  task Flamenco will tell it to render to
  `F:\flamenco\renders\shot_010_a_anim`.
- Carol's computer is also running a worker, but on **Linux**. When it receives a
  render task, Flamenco will tell it to render to
  `/media/shared/flamenco/renders/shot_010_a_anim`.
  
*(Note that the variable name `my_storage` is arbitrary and can be customized as needed.)*

## Ensuring Consistency
It is crucial to ensure that the value of your two-way variable for the platform on which Flamenco Manager is running matches the `shared_storage_path` in your Flamenco Manager configuration. This allows Flamenco Manager to correctly identify and use the variable.

### Example Configuration:
```yaml
_meta:
  version: 3
manager_name: Flamenco
database: flamenco-manager.sqlite
database_check_period: 10m0s
listen: 8080
autodiscoverable: true
local_manager_storage_path: ./flamenco-manager-storage
shared_storage_path: /media/shared/flamenco
shaman:
  enabled: false
  garbageCollect:
    period: 24h0m0s
    maxAge: 744h0m0s
    extraCheckoutPaths: []
task_timeout: 10m0s
worker_timeout: 1m0s
blocklist_threshold: 3
task_fail_after_softfail_count: 3
mqtt:
  client:
    broker: ""
    clientID: flamenco
    topic_prefix: flamenco
    username: ""
    password: ""
variables:
  blender:
    values:
    - platform: linux
      value: blender
    - platform: windows
      value: blender
    - platform: darwin
      value: blender
  blenderArgs:
    values:
    - platform: all
      value: -b -y
  my_storage:
    is_twoway: true
    values:
    - platform: linux
      value: /media/shared/flamenco
    - platform: windows
      value: F:\flamenco
    - platform: darwin
      value: /Volumes/shared/flamenco
```

## Testing Two-way Variables
To verify that your two-way variables are correctly configured, try refresing the connection to flamenco-manager in the Blender add-on preferences. If the Job Storage field displays the correct path for your platform (e.g., F:\flamenco for Windows), your configuration is working as expected.